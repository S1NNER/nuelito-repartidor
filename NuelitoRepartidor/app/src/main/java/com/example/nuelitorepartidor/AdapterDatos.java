package com.example.nuelitorepartidor;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterDatos extends RecyclerView.Adapter<AdapterDatos.ViewHolder>
{


    public List<PedidoPojo> listPedidos;


    public AdapterDatos(List<PedidoPojo> listPedidos){
        this.listPedidos = listPedidos;
    }

    @NonNull
    @Override
    public AdapterDatos.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pedidos_list, null, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        holder.nNombre.setText(listPedidos.get(position).getNombre());
        holder.nTelefono.setText(listPedidos.get(position).getTelefono());

    }

    @Override
    public int getItemCount() {
        return listPedidos.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{


        public TextView nNombre, nTelefono;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), InfoActivity.class);
                    i.putExtra("Nombre", listPedidos.get(getAdapterPosition()).getNombre());
                    i.putExtra("Telefono", listPedidos.get(getAdapterPosition()).getTelefono());
                    i.putExtra("Productos", listPedidos.get(getAdapterPosition()).getProductos());
                    i.putExtra("Total", listPedidos.get(getAdapterPosition()).getTotal());
                    i.putExtra("Latitud", listPedidos.get(getAdapterPosition()).getLatitud());
                    i.putExtra("Longitud", listPedidos.get(getAdapterPosition()).getLongitud());
                    i.putExtra("Id", listPedidos.get(getAdapterPosition()).userId);
                    v.getContext().startActivity(i);
                }
            });
            nNombre = (TextView) itemView.findViewById(R.id.NombrePed);
            nTelefono = (TextView) itemView.findViewById(R.id.Telefono);

        }
    }


}
