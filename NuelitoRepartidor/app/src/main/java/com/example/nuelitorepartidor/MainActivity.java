package com.example.nuelitorepartidor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView ViewPedidos;

    private FirebaseFirestore firebaseFirestore;
    private List <PedidoPojo> listPedidos;
    private AdapterDatos adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listPedidos = new ArrayList<>();
        adapter = new AdapterDatos(listPedidos);

        ViewPedidos = (RecyclerView) findViewById(R.id.lPedidos);
        ViewPedidos.setHasFixedSize(true);
        ViewPedidos.setLayoutManager(new LinearLayoutManager(this));
        ViewPedidos.setAdapter(adapter);

        firebaseFirestore = FirebaseFirestore.getInstance();

        firebaseFirestore.collection("Pedidos").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                if(e != null){

                }
                for(DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()){

                    if(doc.getType() == DocumentChange.Type.ADDED){

                        PedidoPojo pedidoPojo = doc.getDocument().toObject(PedidoPojo.class).withId(doc.getDocument().getId());
                        listPedidos.add(pedidoPojo);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

    }

    }

