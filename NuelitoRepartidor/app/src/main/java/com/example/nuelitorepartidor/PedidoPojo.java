package com.example.nuelitorepartidor;

import java.util.List;

public class PedidoPojo extends DocId {

    public PedidoPojo(){

    }

   private String Nombre, Telefono, Latitud, Longitud, Total, Productos;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getProductos() {
        return Productos;
    }

    public void setProductos(String productos) {
        Productos = productos;
    }

    public PedidoPojo(String nombre, String telefono, String latitud, String longitud, String total, String productos) {
        Nombre = nombre;
        Telefono = telefono;
        Latitud = latitud;
        Longitud = longitud;
        Total = total;
        Productos = productos;
    }
}
