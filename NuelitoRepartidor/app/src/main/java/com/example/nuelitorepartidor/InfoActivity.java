package com.example.nuelitorepartidor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;

public class InfoActivity extends AppCompatActivity{

    TextView tNombre, tTelefono, tProductos, tTotal;
    Button Borrar, Mapa;
    ImageButton Llamar;
    String Latitud, Longitud, Telefono;
    String Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalles);

        tNombre = findViewById(R.id.tNombre);
        tTelefono = findViewById(R.id.tTelefono);
        tProductos = findViewById(R.id.tProductos);
        tTotal = findViewById(R.id.tTotal);

        Intent i = getIntent();
        String Nombre = i.getStringExtra("Nombre");
        Telefono = i.getStringExtra("Telefono");
        String Productos = i.getStringExtra("Productos");
        String Total = i.getStringExtra("Total");
        Latitud = i.getStringExtra("Latitud");
        Longitud = i.getStringExtra("Longitud");
        Id = i.getStringExtra("Id");

        tNombre.setText(Nombre);
        tTelefono.setText(Telefono);
        tProductos.setText(Productos);
        tTotal.setText(Total);

        Mapa = findViewById(R.id.bMapa);
        Mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),MapsActivity.class);
                intent.putExtra("Latitud", Latitud);
                intent.putExtra("Longitud", Longitud);
                v.getContext().startActivity(intent);
            }
        });

        Borrar = findViewById(R.id.bBorrar);
        Borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseFirestore.getInstance().collection("Pedidos").document(Id).delete();
                toast();
            }
        });

        Llamar = findViewById(R.id.ibLlamar);
        Llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + Telefono));
                startActivity(intent);
            }
        });
    }

    void toast(){
        Toast.makeText(this, "PRODUCTO ENTREGADO", Toast.LENGTH_LONG).show();
    }

}
