package com.example.nuelitorepartidor;

import androidx.annotation.NonNull;

public class DocId {
    public String userId;

    public<T extends DocId> T withId(@NonNull final String id){
        this.userId = id;
        return(T) this;
    }
}
